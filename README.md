API
===

[![][OAS badge]][OpenAPI 3.0.3]
[![][version badge]][repository-0.1.0]

[OAS badge]: https://img.shields.io/badge/OAS-3.0.3-green.svg
[OpenAPI 3.0.3]: http://spec.openapis.org/oas/v3.0.3
[repository-0.1.0]: https://gitlab.com/lcg/neuro/v2/ieeevis2020/api/tags/0.1.0
[version badge]: https://img.shields.io/badge/version-0.1.0-yellow.svg

