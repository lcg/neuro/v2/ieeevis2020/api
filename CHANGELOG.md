# Changelog

All notable changes to this project will be documented in this file.
The format is based on [Keep a Changelog], and this project adheres to [Semantic Versioning].

[Keep a Changelog]: https://keepachangelog.com/en/1.0.0/
[Semantic Versioning]: https://semver.org/spec/v2.0.0.html
[Unreleased]: https://gitlab.com/lcg/neuro/v2/ieeevis2020/api/compare?from=release&to=master

## [Unreleased]

### Added

* Paths
  * `default`
    * `GET /location`
    * `GET /version`
* Components
  * Schemas
    * `Electrode`
    * `Id`
    * `Location`
    * `PhysicalQuantity`
    * `PointXY`
    * `Slice`
    * `Version`

